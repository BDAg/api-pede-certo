/*
 * This is needed because Typeorm does not create
 * the migrations table, so when you run migrations
 * after a schema:sync, it will try to run old migrations
 * that are not needed anymore
 */

import getTypeORMConnection from "../src/getTypeORMConnection"
import { Table } from "typeorm"
import fs from "fs"

async function run() {
  const migrations = fs.readdirSync("./src/migrations").map(path => {
    const migrationScript = require(`../src/migrations/${path}`)
    return Object.keys(migrationScript)[0]
  })

  const connection = await getTypeORMConnection()
  const queryRunner = connection.createQueryRunner()

  await queryRunner.createTable(
    new Table({
      name: "migrations",
      columns: [
        {
          name: "id",
          type: "integer",
          isGenerated: true,
          generationStrategy: "increment",
          isPrimary: true,
          isNullable: false
        },
        {
          name: "timestamp",
          type: "bigint",
          isPrimary: false,
          isNullable: false
        },
        {
          name: "name",
          type: "varchar",
          isNullable: false
        }
      ]
    })
  )

  const now = Date.now()

  await connection.query(
    `INSERT INTO migrations ("timestamp", "name") VALUES ${migrations.map(
      migration => `(${now}, '${migration}')`
    )}`
  )

  await connection.synchronize()
}

run().then(
  () => process.exit(0),
  error => {
    console.log(error)
    process.exit(1)
  }
)
