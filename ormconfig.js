const path = require("path")
const { environment, isTest, isProduction } = require("./environment")

const databaseUrl = process.env.DATABASE_URL

const sourceFolder = path.join(
    __dirname,
    {
      development: "/src",
    }[environment]
)

const entities = isTest
    ? [
      path.join(sourceFolder, "/entity/**/!(*.test).js"),
      path.join(sourceFolder, "/test/entity/**/*.js")
    ]
    : [path.join(sourceFolder, "/entity/**/!(*.test).js")]

const config = {
  type: "postgres",
  username: "postgres",
  password: "root",
  dropSchema: isTest,
  synchronize: isTest,
  logging: process.env.LOG_TYPEORM === "true",
  entities,
  extra: {
    ssl: isProduction,
    min: 1,
    max:
        (process.env.DB_POOL_LIMIT && parseInt(process.env.DB_POOL_LIMIT)) || 10
  },
  migrations: [path.join(sourceFolder, "/migrations/*.js")],
  cli: {
    migrationsDir: "src/migrations"
  }
}

if (databaseUrl) {
  config.url = databaseUrl
} else {
  const databaseName = {
    development: "pedecerto"
  }[environment]
  config.database = databaseName
}

module.exports = config
