# Appoia API

## Stack
install postgreSQL 11.5
install Yarn

Once you have everything you need installed, you should create the
databases needed to run and test the project:

```
$ createdb pedecerto
```

Then run:

$ yarn install
```
$ yarn database:setup
```
```
$ yarn dev
```

GraphQL Playground should be running at http://localhost:4000/


* `yarn dev` - start in development mode
* `yarn migration:generate <name>`  - creates a new database migration
named <name> (see below)
* `yarn migration:run` - runs migrations
* `yarn migration:revert` - revert previous migration
* `yarn database:setup` - minimum db setup
* `yarn database:drop` - drops development and test databases


### Generating migrations

TypeORM has a weird way of handling migrations. To create a migration:
1. Change the entity on `src/entity` to what you want.
1. Run `yarn migration:generate MyMigration`
1. This will generate a typescript file on src/migrations. Rename it to
   a javascript file.
1. Open up the file and remove the typescript clutter. Also make sure to
   remove any database changes TypeORM is inadvertly considering new.
1. After all this clean up, just run `yarn migration:run`.

That's it! The database should be updated.

### API

To create an admin user
Run in GraphQL Playground:
```
mutation{
  createAdmin(input:{
  email: "mail@example.com"
  password: "123456"
  name: "example"
  role: "ADMIN" 
  }){
    id
  }
}
```
To get the authentication token
```
mutation{
  login(email:"mail@example.com"
    		password: "123456")
}
```
Creation example
```
mutation{
  createProduto(input:{
  nome: "Biscoito Maizena"
  descricao: "Biscoito maizena tradicional caixa 500G"
  preco: 25.9
  image_src: "aqui.jpg"
  }){
    id
  }
}
```
Update example
```
mutation{
  updateProduto(id:2
    input: {
    nome: "Biscoito Maizena 2"
  	descricao: "caixa 500G bistoico maizena"
  	preco: 25.9
  	image_src: "aqui.jpg"
    
    }){id}
}
```
Delete example
```
mutation{
  deleteProduto(id:1){id}
}
```
List example
```
query{
	produtos(pageSize:10){edges{node{id,nome}}}
}
```
Header Authentication 
```
{
  "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1c2VySWQiOjEsInNlc3Npb25JZCI6Ijg4NjcxMDcyNjM4NzExOTcwIn0.UveUYpsEvG_IcHbKOwzgErq2ssMD5zG2eLvXjuRWGe2BfyfR1sq26uVABCZKuF415R-3rTgwBtHXZt_k9Xk1coptgVJysiSnVWAbRv03I7uSLFVZRLZLHNrjnVq6osi2laPdtepzuoSJn21SdqX_aTWsKL8GGWKy7-MsaRJNoOU"
}
```





