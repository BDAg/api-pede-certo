export default function basicAuth(username, password) {
  return (req, res, next) => {
    const auth = require("basic-auth")
    const credentials = auth(req)

    if (
      !credentials ||
      credentials.name !== username ||
      credentials.pass !== password
    ) {
      res.statusCode = 401
      res.setHeader("WWW-Authenticate", 'Basic realm="Private"')
      res.end("Access denied")
    } else {
      next()
    }
  }
}
