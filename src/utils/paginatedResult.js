export default function paginatedResult(totalCount, page, pageSize, records) {
  const pageIndex = page - 1
  const pageCount = Math.ceil(totalCount / pageSize)
  return {
    totalCount,
    pageInfo: {
      page,
      pageSize,
      pageCount,
      hasNextPage: page * pageSize < totalCount,
      hasPrevPage: pageIndex > 0
    },
    edges: records.map(node => ({ node }))
  }
}
