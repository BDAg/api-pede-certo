import chalk from "chalk"
import httpContext from "express-http-context"

let currentLevel

const levels = ["silly", "debug", "verbose", "info", "warn", "error"]

const colors = {
  info: chalk.cyan,
  warn: chalk.yellow,
  error: chalk.red
}

if (process.env.NODE_ENV === "test") {
  setLevel("error")
} else {
  setLevel(process.env.LOG_LEVEL)
}

function log(level, ...args) {
  if (levels.indexOf(level) < currentLevel) {
    return
  }

  const reqId = httpContext.get("req-id")

  const reqIdOutput = reqId ? `[${reqId}]` : ""

  const levelColor = colors[level] ? colors[level] : f => f

  console.log(levelColor(`[${level}]`), reqIdOutput, ...args)
}

function setLevel(level) {
  const newLevel = levels.indexOf(level)
  currentLevel = newLevel === -1 ? levels.indexOf("info") : newLevel
}

const logger = {
  setLevel,
  silly: log.bind(null, "silly"),
  debug: log.bind(null, "debug"),
  verbose: log.bind(null, "verbose"),
  info: log.bind(null, "info"),
  warn: log.bind(null, "warn"),
  error: log.bind(null, "error")
}

export default logger
