import { getRepository } from "typeorm"
import { validate } from "class-validator"
import BadInputError from "../errors/BadInputError"
import NotFoundError from "../errors/NotFoundError"
import setAttributes from "./setAttributes"

export default function updateAction(Entity, filterByUser = false) {
  return async (_, { id, input }, { user }) => {
    const record = await getRepository(Entity).findOne({
      id,
      ...(filterByUser ? { user } : {})
    })
    if (!record) {
      throw new NotFoundError(Entity, id)
    }
    setAttributes(record, input)
    const errors = await validate(record)
    if (errors.length) {
      throw new BadInputError(errors)
    } else {
      await record.save()
      return record
    }
  }
}
