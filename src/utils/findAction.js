import { getRepository, IsNull } from "typeorm"
import NotFoundError from "../errors/NotFoundError"

export default function findAction(Entity, options) {
  const { filterByUser, idColumn, deletedAt } = Object.assign(
    {},
    { filterByUser: false, idColumn: "id", deletedAt: "deletedAt" },
    options
  )

  return async (_, args, { user }) => {
    const id = args[idColumn]
    const record = await getRepository(Entity).findOne({
      [idColumn]: id,
      [deletedAt]: IsNull(),
      ...(filterByUser ? { user } : {})
    })
    if (!record) {
      throw new NotFoundError(Entity, id)
    }
    return record
  }
}
