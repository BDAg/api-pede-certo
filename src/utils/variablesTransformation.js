import { merge } from "lodash/fp"

export default function variablesTransformation(transformations) {
  return resolver => async (parent, variables, context, info) => {
    const transformationResults = await Promise.all(
      transformations.map(transformation => transformation(variables, context))
    )
    const transformedVariables = transformationResults.reduce(
      (acc, result) => merge(acc, result),
      variables
    )
    return resolver(parent, transformedVariables, context, info)
  }
}
