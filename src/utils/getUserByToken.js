import * as jwt from "jwt-simple"
import User from "../entity/User"
import logger from "./logger"

export default async function getUserByToken(token) {
  try {
    const payload = jwt.decode(
      token,
      process.env.JWT_PUBLIC_KEY.replace(/\\n/g, "\n"),
      false,
      "RS256"
    )
    const user = await User.findOne(payload.userId)
    if ((user && user.sessionId === payload.sessionId) || user.admin) {
      return user
    }
  } catch (err) {
    logger.error("Problem handling authorization token: ", err)
  }
}
