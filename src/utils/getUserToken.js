import * as jwt from "jwt-simple"

export default function getUserToken(user) {
  return jwt.encode(
    {
      userId: user.id,
      sessionId: user.sessionId
    },
    process.env.JWT_SECRET_KEY.replace(/\\n/g, "\n"),
    "RS256"
  )
}
