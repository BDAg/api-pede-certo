import DataLoader from "dataloader"

export function getDataLoader(context, loader, key) {
  if (!context.dataLoaders) {
    context.dataLoaders = {}
  }

  if (!context.dataLoaders[key]) {
    context.dataLoaders[key] = new DataLoader(loader)
  }

  return context.dataLoaders[key]
}
