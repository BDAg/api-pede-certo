import getUserByToken from "./getUserByToken"
import logger from "./logger"

export default async function getRequestUser(req) {
  logger.silly("Access from", req.ips)
  if (req.headers.authorization) {
    const authorizationParts = req.headers.authorization.split(" ")
    const authenticationType = authorizationParts[0]
    if (authenticationType === "Bearer") {
      const user = await getUserByToken(authorizationParts[1])
      return user || null
    }
  }
}
