import {
  has,
  mapKeys,
  range,
  includes,
  flow,
  filter,
  reduce,
  keys,
  map,
  split,
  get,
  merge
} from "lodash/fp"
import Aigle from "aigle"
import { getRepository, IsNull } from "typeorm"
import paginatedResult from "./paginatedResult"

export const ENTITY_ALIAS = "entity"

export default function indexAction(
  Entity,
  orderBy = { id: "ASC" },
  filters = [],
  { defaultVariables } = { defaultVariables: { deletedAt: null } }
) {
  return async (_, variables, { user }) => {
    const filterVariables = merge(variables, defaultVariables)
    const { page = 1, pageSize = 10 } = filterVariables
    const pageIndex = page - 1
    const queryBuilder = await getRepository(Entity)
      .createQueryBuilder(ENTITY_ALIAS)
      .where({ deletedAt: IsNull() })
      .orderBy(
        mapKeys(
          fieldName =>
            includes(".", fieldName)
              ? fieldName
              : `${ENTITY_ALIAS}.${fieldName}`,
          orderBy
        )
      )
      .offset(pageIndex * pageSize)
      .limit(pageSize)

    const joinedQuery = flow(
      keys,
      filter(includes(".")),
      map(split(".")),
      map(get("0")),
      reduce(
        (query, association) =>
          query.leftJoinAndSelect(
            `${ENTITY_ALIAS}.${association}`,
            association
          ),
        queryBuilder
      )
    )(orderBy)

    const presentFilters = filters.filter(filter =>
      has(filter.fieldName, filterVariables)
    )

    const filteredQuery = await Aigle.resolve(
      range(0, presentFilters.length)
    ).reduce((query, filterIndex) => {
      const { fieldName, filter } = presentFilters[filterIndex]
      return filter(query, filterVariables[fieldName], filterIndex)
    }, joinedQuery)

    const totalCount = await filteredQuery.getCount()
    const records = await filteredQuery.getMany()

    return paginatedResult(totalCount, page, pageSize, records)
  }
}
