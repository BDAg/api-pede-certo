export default function generateListType(type, maxAge = 0) {
  return `
    type ${type}List @cacheControl(maxAge: ${maxAge}) {
      totalCount: Int!
      pageInfo: PageInfo!
      edges: [${type}Edge!]!
    }

    type ${type}Edge @cacheControl(maxAge: ${maxAge}) {
      node: ${type}!
    }
  `
}
