import { flow, keys, each } from "lodash/fp"

// This method needs to be used when setting entity attributes
// because typeorm `create` and other methods won't set the
// virtual fields (fields not listed as columns)
export default function setAttributes(record, attributes) {
  flow(
    keys,
    each(key => {
      record[key] = attributes[key]
    })
  )(attributes)
}
