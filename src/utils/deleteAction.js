import { getRepository } from "typeorm"
import NotFoundError from "../errors/NotFoundError"

export default function deleteAction(Entity, filterByUser = false) {
  return async (_, { id }, { user }) => {
    const record = await getRepository(Entity).findOne({
      id,
      ...(filterByUser ? { user } : {})
    })
    if (!record) {
      throw new NotFoundError(Entity, id)
    }
    record.deletedAt = new Date().toISOString()
    await record.save()
    return record
  }
}
