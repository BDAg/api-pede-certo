import { get, set } from "lodash/fp"

export default function transformVariable(
  fromVariable,
  toVariable,
  transformation
) {
  return async variables => {
    const fromValue = get(fromVariable, variables)
    if (fromValue !== undefined) {
      const toValue = await transformation(fromValue, variables)
      return set(toVariable, toValue, variables)
    }
  }
}
