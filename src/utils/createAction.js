import { getRepository } from "typeorm"
import { validate } from "class-validator"
import BadInputError from "../errors/BadInputError"
import setAttributes from "./setAttributes"

export default function createAction(Entity) {
  return async (_, { input }, { user }) => {
    const record = getRepository(Entity).create({})
    record.user = user
    setAttributes(record, input)
    const errors = await validate(record)
    if (errors.length) {
      throw new BadInputError(errors)
    } else {
      await record.save()
      return record
    }
  }
}
