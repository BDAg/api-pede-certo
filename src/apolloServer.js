import { ApolloServer } from "apollo-server-express"
import { defaultErrorFormatter } from "./utils/defaultErrorFormatter"
import { pick } from "lodash/fp"
import { typeDefs, resolvers, directives as schemaDirectives } from "./schema"
import getRequestUser from "./utils/getRequestUser"

const context = async function context({ req: request }) {
  const user = await getRequestUser(request)
  return { user, request }
}

const apolloConfig = {
  typeDefs,
  resolvers,
  schemaDirectives,
  context,
  formatError,
  introspection: process.env.GRAPHQL_INTROSPECTION_ENABLED === "true",
  playground: process.env.GRAPHQL_PLAYGROUND_ENABLED === "true"
}

function formatError(error) {
    console.error(error)
  if (error.extensions.exception) {
    error.extensions.exception = pick(["code"], error.extensions.exception)
  }
  return defaultErrorFormatter(error)
}

const server = new ApolloServer(apolloConfig)

export default server
export { typeDefs, resolvers, schemaDirectives, context }
