import { flow, map, keyBy, get } from "lodash/fp"
import path from "path"

const jobPaths = []

require("fs")
  .readdirSync(__dirname)
  .forEach(file => {
    if (file.endsWith("Job.js")) {
      jobPaths.push(path.join(__dirname, file))
    }
  })

export default flow(
  map(require),
  map(get("default")),
  keyBy("name")
)(jobPaths)
