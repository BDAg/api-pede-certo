import { map } from "lodash/fp"
import Arena from "bull-arena"
import jobs from "./jobs"

export default Arena(
  {
    queues: map(queueConfig, jobs)
  },
  {
    basePath: "/arena",
    disableListen: true
  }
)

function queueConfig(job) {
  return {
    name: job.queueName,
    hostId: "pede-certo",
    redis: process.env.REDIS_URL
  }
}
