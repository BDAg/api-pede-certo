import { ENTITY_ALIAS } from "../utils/indexAction"
import { Brackets } from "typeorm"

const splitValues = values => {
  const greaterThanValues = []
  const inValues = []
  values.forEach(value => {
    if (value.endsWith("+")) {
      greaterThanValues.push(value.slice(0, -1))
    } else {
      inValues.push(value)
    }
  })
  return [greaterThanValues, inValues]
}

// Given an array of numbers ["1", "2", "3+"] it will filter data which matches 1 or 2 or are >= 3

export default function filterByNumber(fieldName, columnName) {
  return {
    fieldName,
    filter: (queryBuilder, values, filterIndex) => {
      const [greaterThanValues, inValues] = splitValues(values)
      const valuesFieldName = `${fieldName}FilterByNumberIn`

      queryBuilder.andWhere(
        new Brackets(query => {
          if (inValues.length) {
            query.andWhere(
              `"${ENTITY_ALIAS}"."${columnName}" IN (:...${valuesFieldName})`,
              {
                [valuesFieldName]: inValues
              }
            )
          }

          if (greaterThanValues.length) {
            greaterThanValues.forEach((value, i) => {
              const varName = `${fieldName}FilterByNumberGt${i}`
              query.orWhere(
                `"${ENTITY_ALIAS}"."${columnName}" >= :${varName}`,
                {
                  [varName]: value
                }
              )
            })
          }
        })
      )

      return queryBuilder
    }
  }
}
