import { flatten } from "lodash"
import joinQueryByAssocationPath from "../utils/joinQueryByAssociationPath"

export default function filterByJoinTable(
  fieldName,
  associationOrAssociations,
  columnName = "id"
) {
  const associations = flatten([associationOrAssociations])
  return {
    fieldName,
    filter: (queryBuilder, valueOrValues, filterIndex) =>
      joinQueryByAssocationPath(
        associations,
        columnName,
        valueOrValues,
        filterIndex,
        queryBuilder
      )
  }
}
