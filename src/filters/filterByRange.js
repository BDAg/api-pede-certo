import { ENTITY_ALIAS } from "../utils/indexAction"

export default function filterByRange(fieldName, columnName) {
  const rangeBeginVar = `${fieldName}Begin`
  const rangeEndVar = `${fieldName}End`

  return {
    fieldName,
    filter: (queryBuilder, range, filterIndex) =>
      queryBuilder.andWhere(
        `"${ENTITY_ALIAS}"."${columnName}" BETWEEN :${rangeBeginVar} AND :${rangeEndVar}`,
        { [rangeBeginVar]: range.begin, [rangeEndVar]: range.end }
      )
  }
}
