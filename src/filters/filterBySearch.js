import { Brackets } from "typeorm"
import { ENTITY_ALIAS } from "../utils/indexAction"

export default function filterBySearch(fieldName, columnNameOrNames) {
  const columnNames = [].concat(columnNameOrNames)

  return {
    fieldName,
    filter: (queryBuilder, valueOrValues, filterIndex) =>
      queryBuilder.andWhere(
        new Brackets(bracket =>
          columnNames.reduce(
            (query, columnName) =>
              query.orWhere(
                `"${ENTITY_ALIAS}"."${columnName}" ILIKE :valueOrValues`,
                { valueOrValues: `%${valueOrValues}%` }
              ),
            bracket
          )
        )
      )
  }
}
