import { includes } from "lodash/fp"
import { ENTITY_ALIAS } from "../utils/indexAction"

export default function filterByPublished(columnName) {
  const joinRequired = includes(".", columnName)

  return {
    fieldName: "published",
    filter: (queryBuilder, published) => {
      const columnPath = joinRequired
        ? `"${associationAlias(columnName)}"."${columnName.split(".")[1]}"`
        : `"${ENTITY_ALIAS}"."${columnName}"`

      const whereClause = published
        ? `${columnPath} <= :now`
        : `${columnPath} IS NULL OR ${columnPath} > :now`

      const joinedQuery = joinRequired
        ? joinQuery(queryBuilder, published)
        : queryBuilder

      return joinedQuery.andWhere(whereClause, { now: new Date() })
    }
  }

  function joinQuery(queryBuilder, published) {
    const association = columnName.split(".")[0]
    if (published) {
      return queryBuilder.innerJoinAndSelect(
        `${ENTITY_ALIAS}.${association}`,
        associationAlias(columnName)
      )
    } else {
      return queryBuilder.leftJoinAndSelect(
        `${ENTITY_ALIAS}.${association}`,
        associationAlias(columnName)
      )
    }
  }

  function associationAlias(columnName) {
    return `${columnName.split(".")[0]}_published`
  }
}
