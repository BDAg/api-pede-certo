import { flatten } from "lodash"
import { ENTITY_ALIAS } from "../utils/indexAction"

export default function filterByColumn(fieldName, columnName) {
  const valuesFieldName = `${fieldName}Values`
  return {
    fieldName,
    filter: (queryBuilder, valueOrValues, filterIndex) =>
      queryBuilder.andWhere(
        `"${ENTITY_ALIAS}"."${columnName}" IN (:...${valuesFieldName})`,
        {
          [valuesFieldName]: flatten([valueOrValues])
        }
      )
  }
}
