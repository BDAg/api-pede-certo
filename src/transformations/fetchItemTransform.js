import NotFoundError from "../errors/NotFoundError"

export default function fetchItemTransform(Entity) {
  return async value => {
    if (value) {
      const result = await Entity.findOne(value)
      if (!result) {
        throw new NotFoundError(Entity, value)
      }
      return result
    } else {
      return null
    }
  }
}
