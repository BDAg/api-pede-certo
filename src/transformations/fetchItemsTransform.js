import NotFoundError from "../errors/NotFoundError"

export default function fetchItemsTransform(Entity, matchColumn = "id") {
  return async ids => {
    if (!Array.isArray(ids)) {
      console.error(
        "Admin: fetchItemsTransform should only be used with a collection of entities. For single entities use fetchItemTransform instead"
      )
    }
    if (ids.length) {
      const results = await Entity.createQueryBuilder("entity")
        .where(`entity."${matchColumn}" IN (:...ids)`, { ids })
        .getMany()
      if (results.length !== ids.length) {
        throw new NotFoundError(Entity, ids)
      }
      return results
    } else {
      return []
    }
  }
}
