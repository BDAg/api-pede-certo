import { find } from "lodash/fp"
import { getDataLoader } from "../utils/getDataLoader"
import { IsNull } from "typeorm"

export default function manyToOneResolver(
  Entity,
  entityIdFieldName,
  relations = []
) {
  const key = `manyToOne-${Entity.name}-${relations.sort().join("_")}`

  return (owner, variables, context) => {
    const id = owner[entityIdFieldName]
    if (id) {
      return getDataLoader(context, loader, key).load(id)
    } else {
      return null
    }
  }

  async function loader(ids) {
    const results = await Entity.findByIds(ids, {
      relations,
      where: { deletedAt: IsNull() }
    })
    return ids.map(id => find({ id }, results))
  }
}
