import { find, filter, flatten } from "lodash/fp"
import { getDataLoader } from "../utils/getDataLoader"
import { getRepository, IsNull } from "typeorm"

export default function manyToManyResolver(Entity, collectionFieldName) {
  const key = `manyToMany-${Entity.name}-${collectionFieldName}`

  return (owner, _, context) =>
    getDataLoader(context, loader, key).load(owner.id)

  async function loader(ownerIds) {
    const results = await getRepository(Entity).findByIds(ownerIds, {
      where: { deletedAt: IsNull() },
      relations: [collectionFieldName]
    })
    const owners = ownerIds.map(
      id => find({ id, deletedAt: null }, results)[collectionFieldName]
    )
    return [filter({ deletedAt: null }, flatten(owners))]
  }
}
