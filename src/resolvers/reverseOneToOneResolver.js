import { keyBy } from "lodash/fp"
import { In, IsNull } from "typeorm"
import { getDataLoader } from "../utils/getDataLoader"

export default function reverseOneToOneResolver(
  Entity,
  relationName,
  relationIdFieldName,
  relations = []
) {
  const key = `reverseOneToOne-${Entity.name}-${relations.sort().join("_")}`

  return (owner, variables, context) => {
    if (owner) {
      return getDataLoader(context, loader, key).load(owner.id)
    } else {
      return null
    }
  }

  async function loader(ids) {
    const results = await Entity.find({
      where: {
        [relationName]: In(ids),
        deletedAt: IsNull()
      },
      relations
    })

    const resultsByRelation = keyBy(relationIdFieldName, results)
    return ids.map(id => resultsByRelation[id])
  }
}
