import fs from "fs"
import express from "express"
import morgan from "morgan"
import httpContext from "express-http-context"
import logger from "./utils/logger"
import getTypeORMConnection from "./getTypeORMConnection"
import arena from "./arena"
import basicAuth from "./utils/basicAuth"
import apolloServer from "./apolloServer"

const port = process.env.PORT || 4000

startServer()

async function startServer() {
  await getTypeORMConnection()

  const app = express()

  app.set("trust proxy", true)

  const morganPreset =  "dev"
  app.use(morgan(morganPreset))
  app.use(httpContext.middleware)
  app.use((req, res, next) => {
    httpContext.set("req-id", req.headers["x-request-id"])
    next()
  })

  app.use(express.urlencoded())
  app.use(express.json())

  app.use("/", arena)

  await apolloServer.applyMiddleware({ app, path: "/" })

  const config = { port }
  app.listen(config, serverStarted)
}

function serverStarted() { 
    logger.info(`Server is running on port: ${port}`)
}
