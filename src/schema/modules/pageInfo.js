export const types = `
  type PageInfo @cacheControl(maxAge: 60) {
    page: Int!
    pageSize: Int!
    pageCount: Int!
    hasNextPage: Boolean!
    hasPrevPage: Boolean!
  }
`
