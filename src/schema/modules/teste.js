
import generateListType from "../../utils/generateListType"
import { textSpanEnd } from "typescript"

export const types =[
    generateListType("Teste"),
    `
        type Teste {
            resultado:String
        }
        extend type Query {
            testar(id: String!): String! @visible(to: PORTAL)
            
        }    
        
    
    `
]

export const resolvers = {
    Query: {
        testar(){
            var soap = require('soap');
            var url = 'https://wsi.mtrix.com.br/wsMarilan.asmx?wsdl';
            var args = {dtInicial :'2019-08-27',
                        dtFinal : '2019-08-27',
                        cdAd:'5514',
                        tpArquivo : 'T'}
            var auth = "Basic " + new Buffer("mtrix\wsMarilan" + ":" + "mtrix$12345").toString("base64");              
            soap.createClient(url, { wsdl_headers: { Authorization: auth } }, function (err, client) {
                client.GetData(args)
                
            });

            return "exito"

        }
    }
}