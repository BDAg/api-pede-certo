import User from "../../entity/Produto"
import Produto from "../../entity/Produto"
import Categoria from "../../entity/Categoria"
import createAction from "../../utils/createAction"
import updateAction from "../../utils/updateAction"
import deleteAction from "../../utils/deleteAction"
import findAction from "../../utils/findAction"
import indexAction from "../../utils/indexAction"
import paginatedResult from "../../utils/paginatedResult"
import generateListType from "../../utils/generateListType"
import variablesTransformation from "../../utils/variablesTransformations"
import transformVariable from "../../utils/transformVariable"
import fetchItemTransform from "../../transformations/fetchItemTransform"
import manyToOneResolver from "../../resolvers/manyToOneResolver"

export const types =[
    generateListType("Produto"),
    `
        type Produto {
            id: ID!
            nome: String!
            descricao: String
            preco: Float!
            altura: Float
            largura: Float
            profundidade: Float
            peso: Float
            image_src: String
            categoria: Categoria
        }
        input CreateProdutoInput {
            nome: String!
            descricao: String
            preco: Float!
            altura: Float
            largura: Float
            profundidade: Float
            peso: Float
            image_src: String
            categoriaId: ID!
            
        }
        input UpdateProdutoInput {
            nome: String
            descricao: String
            preco: Float
            image_src: String
            altura: Float
            largura: Float
            profundidade: Float
            peso: Float
            categoriaId: ID
        }
        extend type Query {
            produto(id: ID!): Produto! @visible(to: PORTAL)
            produtos(page: Int = 1
            pageSize: Int = 10
            ): ProdutoList! @visible(to: PORTAL)
          
            findProdutoByCategoria(
                page: Int = 1
                pageSize: Int = 10
                categoriaId: ID!
              ): ProdutoList! @visible(to: PORTAL)
        }    
        
        extend type Mutation {
            createProduto(input: CreateProdutoInput!): Produto! @visible(to: ADMIN)
            updateProduto(id: ID!, input: UpdateProdutoInput!): Produto! @visible(to: ADMIN)
            deleteProduto(id: ID!): Produto! @visible(to: ADMIN)
        }
    `
]

const transformInput = variablesTransformation([
    transformVariable("input.categoriaId", "input.categoria", fetchItemTransform(Categoria))
])

const createActionProduto = transformInput(createAction(Produto))

const updateProduto = updateAction(Produto)
const createProduto = async (_, { input }, { categoria }) => {
    const Produto = await createActionProduto(_,{input},{categoria})
    return Produto
}



const findProdutoByCategoria = async (_, args) => {
    const res = await Produto.find({where:{categoria: args.categoriaId, deletedAt:null}})
  
    return paginatedResult(res.length,args.page,args.pageSize,res)
  }

export const resolvers = {
  Query: {
    produto: findAction(Produto),
    findProdutoByCategoria,
    produtos: indexAction(User, { id: "ASC" })
  },
  Mutation: {
    createProduto,
    updateProduto,
    deleteProduto: deleteAction(Produto),
   },
   Produto: {
    categoria: manyToOneResolver(Categoria, "categoriaId")
  }
}
