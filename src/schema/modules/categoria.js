import Categoria from "../../entity/Categoria"
import createAction from "../../utils/createAction"
import updateAction from "../../utils/updateAction"
import deleteAction from "../../utils/deleteAction"
import findAction from "../../utils/findAction"
import indexAction from "../../utils/indexAction"
import generateListType from "../../utils/generateListType"
import filterByColumn from "../../filters/filterByColumn"
import filterBySearch from "../../filters/filterBySearch"
import paginatedResult from "../../utils/paginatedResult"

export const types =[
    generateListType("Categoria"),
    `
        type Categoria {
            id: ID!
            nome: String!
            descricao: String
        }
        input CreateCategoriaInput {
            nome: String!
            descricao: String
        }
        input UpdateCategoriaInput {
            nome: String!
            descricao: String
        }
        extend type Query {
            categoria(id: ID!): Categoria! @visible(to: PORTAL)
            categorias(page: Int = 1
            pageSize: Int = 10
            ): CategoriaList! @visible(to: PORTAL)
          
            filtroCategorias(
              page: Int = 1
              pageSize: Int = 10
              nome: String
              descricao: String
            ): CategoriaList! @visible(to: PORTAL)
        }    
        
        extend type Mutation {
            createCategoria(input: CreateCategoriaInput!): Categoria! @visible(to: ADMIN)
            updateCategoria(id: ID!, input: UpdateCategoriaInput!): Categoria! @visible(to: ADMIN)
            deleteCategoria(id: ID!): Categoria! @visible(to: ADMIN)
        }
    `
]

const updateCategoria = updateAction(Categoria)
const createCategoria = createAction(Categoria)

const filtroCategorias = async (_, args) => {
    const res = await Categoria.find({where:{nome: args.nome, descricao:args.descricao}})

    return paginatedResult(res.length,args.page,args.pageSize,res)
}

export const resolvers = {
    Query: {
        categoria: findAction(Categoria),
        filtroCategorias,
        categorias: indexAction(Categoria, { id: "ASC" })
    },
    Mutation: {
        createCategoria,
        updateCategoria,
        deleteCategoria: deleteAction(Categoria),
    }
}
