import path from "path"

const modules = []

require("fs")
  .readdirSync(__dirname)
  .forEach(function(file) {
    if (file === "index.js" || file.endsWith(".test.js")) return
    modules.push(require(path.join(__dirname, file)))
  })

export default modules
