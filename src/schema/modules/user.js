import User from "../../entity/User"
import createAction from "../../utils/createAction"
import updateAction from "../../utils/updateAction"
import deleteAction from "../../utils/deleteAction"
import findAction from "../../utils/findAction"
import indexAction from "../../utils/indexAction"
import generateListType from "../../utils/generateListType"
import filterByColumn from "../../filters/filterByColumn"
import filterBySearch from "../../filters/filterBySearch"
import getUserToken from "../../utils/getUserToken"

export const types = [
  generateListType("User"),
  `
    enum UserRoleType {
      DEALER
      ADMIN
    }

    enum AllUserRoleType {
      SELLER
      ADMIN
    }

    type User {
      id: ID!
      email: String!
      name: String!
      cnpj: String!
      role: AllUserRoleType!
      phone: String
    }

    input CreateUserInput {
      email: String!
      password: String!
      name: String!
      cnpj: String!
      role: UserRoleType!
      phone: String
    }

    input CreateAdminInput {
      email: String!
      password: String!
      name: String!
      role: String = "ADMIN"
    }

    input UpdateAdminInput {
      email: String
      password: String
      name: String
      role: String = "ADMIN"
    }

    input UpdateUserInput {
      email: String
      password: String
      name: String
      role: UserRoleType
      phone: String
      cnpj: String
    }

    input UpdateUserProfileInput {
      name: String
      cnpj: String
      phone: String
    }

    extend type Query {
      me: User! @visible(to: PORTAL)
      user(id: ID!): User! @visible(to: ADMIN)

      users(
        page: Int = 1
        pageSize: Int = 10
        role: [AllUserRoleType]
        search: String
      ): UserList! @visible(to: ADMIN)
    }

    extend type Mutation {
      createUser(input: CreateUserInput!): User! @visible(to: PUBLIC)
      createAdmin(input: CreateAdminInput!): User! @visible(to: PUBLIC)
      updateUser(id: ID!, input: UpdateUserInput!): User! @visible(to: ADMIN)
      updateUserProfile(id: ID!, input: UpdateUserProfileInput!): User! @visible(to: PORTAL)
      deleteUser(id: ID!): User! @visible(to: ADMIN)

      login(
        email: String!
        password: String!
      ): String @visible(to: PUBLIC)

      logout: Boolean! @visible(to: PUBLIC)

    }
  `
]

const updateUserProfile = updateAction(User)
const updateUser = updateAction(User)
const createUser = createAction(User)


export const resolvers = {
  Query: {
    user: findAction(User),
    users: indexAction(User, { id: "ASC" }, [
      filterByColumn("role", "role"),
      filterBySearch("search", ["name", "email"])
    ]),
    me(_, __, { user }) {
      return user
    }
  },
  Mutation: {
    createUser,
    createAdmin: createUser,
    updateUserProfile,
    updateUser,
    deleteUser: deleteAction(User),
    async login(_, { email, password }, { request }) {
      const user = await User.findByEmailForAuthentication(email)

      if (user && (await user.validatePassword(password))) {
        if (!user.sessionId) {
          await user.generateSessionId()
        }

        return getUserToken(user)
      }

      return null
    },
    async logout(root, args, { user }) {
      await user.removeSessionId()
      return true
    }
   }
}
