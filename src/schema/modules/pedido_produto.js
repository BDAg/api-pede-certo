import User from "../../entity/Produto"
import createAction from "../../utils/createAction"
import updateAction from "../../utils/updateAction"
import deleteAction from "../../utils/deleteAction"
import findAction from "../../utils/findAction"
import indexAction from "../../utils/indexAction"
import generateListType from "../../utils/generateListType"
import filterByColumn from "../../filters/filterByColumn"
import filterBySearch from "../../filters/filterBySearch"
import Pedido_Produto from "../../entity/Pedido_Produto"
import Pedido from "../../entity/Pedido"
import Produto from "../../entity/Produto"
import paginatedResult from "../../utils/paginatedResult"
import manyToOneResolver from "../../resolvers/manyToOneResolver"
import variablesTransformation from "../../utils/variablesTransformation"
import transformVariable from "../../utils/transformVariable"
import fetchItemTransform from "../../transformations/fetchItemTransform"

export const types =[
    generateListType("Pedido_Produto"),
    `
        type Pedido_Produto {
            id: ID!
            pedido:Pedido!
            preco: Float
            produto:Produto!
            quantidade:Int
        }
        input CreatePedido_ProdutoInput {
            pedidoId:ID!
            preco: Float!
            produtoId:ID!
            quantidade:Int!
        }
        input UpdatePedido_ProdutoInput {
            pedidoId:ID!
            preco: Float!
            produtoId:ID!
            quantidade:Int!
        }
        extend type Query {
            pedido_produto(id: ID!): Pedido_Produto! @visible(to: PORTAL)
            pedido_produtos(page: Int = 1
            pageSize: Int = 10
            ): Pedido_ProdutoList! @visible(to: PORTAL)
          
            findProdutoByPedido(
              page: Int = 1
              pageSize: Int = 10
              pedidoId: ID!
            ): Pedido_ProdutoList! @visible(to: PORTAL)
        }    
        
        extend type Mutation {
            createPedido_Produto(input: CreatePedido_ProdutoInput!): Pedido_Produto! @visible(to: PORTAL)
            updatePedido_Produto(id: ID!, input: UpdatePedido_ProdutoInput!): Pedido_Produto! @visible(to: PORTAL)
            deletePedido_Produto(id: ID!): Pedido_Produto! @visible(to: PORTAL)
        }
    `
]

//const updatePedido_Produto = updateAction(Pedido_Produto)
//const createPedido_Produto = createAction(Pedido_Produto)


const transformInput = variablesTransformation([
  transformVariable("input.produtoId", "input.produto", fetchItemTransform(Produto)),
  transformVariable("input.pedidoId", "input.pedido", fetchItemTransform(Pedido))
])

const createActionPedido_Produto = transformInput(createAction(Pedido_Produto))
const updateActionPedido_Produto = transformInput(createAction(Pedido_Produto))

const createPedido_Produto = async (_, { input }, { user }) => {
  const Pedido_Produto = await createActionPedido_Produto(_,{input},{user})
  return Pedido_Produto
}

const updatePedido_Produto = async (_, { input }, { user }) => {
  const Pedido = await updateActionPedido_Produto(_,{input},{user})
  return Pedido
}

const findProdutoByPedido = async (_, args) => {
  const res = await Pedido_Produto.find({where:{pedido: args.pedidoId, deletedAt:null}})

  return paginatedResult(res.length,args.page,args.pageSize,res)
}

export const resolvers = {
  Query: {
    pedido_produto: findAction(Pedido_Produto),
    findProdutoByPedido,
    pedido_produtos: indexAction(Pedido_Produto, { id: "ASC" })
  },
  Mutation: {
    createPedido_Produto,
    updatePedido_Produto,
    deletePedido_Produto: deleteAction(Pedido_Produto),
   },
   Pedido_Produto: {
    produto: manyToOneResolver(Produto, "produtoId"),
    pedido: manyToOneResolver(Pedido, "pedidoId")
  }
}