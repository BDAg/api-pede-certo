import Pedido from "../../entity/Pedido"
import createAction from "../../utils/createAction"
import updateAction from "../../utils/updateAction"
import deleteAction from "../../utils/deleteAction"
import findAction from "../../utils/findAction"
import indexAction from "../../utils/indexAction"
import generateListType from "../../utils/generateListType"
import filterByColumn from "../../filters/filterByColumn"
import filterBySearch from "../../filters/filterBySearch"
import User from "../../entity/User"
import paginatedResult from "../../utils/paginatedResult"
import variablesTransformation from "../../utils/variablesTransformation"
import transformVariable from "../../utils/transformVariable"
import fetchItemsTransform from "../../transformations/fetchItemsTransform"
import fetchItemTransform from "../../transformations/fetchItemTransform"
import Produto from "../../entity/Produto"
import manyToManyResolver from "../../resolvers/manyToManyResolver"
import manyToOneResolver from "../../resolvers/manyToOneResolver"
import reverseOneToOneResolver from "../../resolvers/reverseOneToOneResolver"
import { isNull } from "util"

export const types =[
    generateListType("Pedido"),
    `
        type Pedido {
            id: ID!
            dealer:User!
            preco_total:Float
            data_pedido: String!
        }
        input CreatePedidoInput {
            dealerId:ID!
            preco_total:Float!
            data_pedido: String!
            
        }
        input UpdatePedidoInput {
            preco_total:Float
            data_pedido: String 
        }
        extend type Query {

            pedido(id: ID!): Pedido! @visible(to: PORTAL)
            
            pedidos(page: Int = 1
            pageSize: Int = 10
            ): PedidoList! @visible(to: PORTAL)
          
            findPedidoByUser(
              userId: ID!
            ): PedidoList! @visible(to: PORTAL)
        }    
        
        extend type Mutation {
            createPedido(input: CreatePedidoInput!): Pedido! @visible(to: PORTAL)
            updatePedido(id: ID!, input: UpdatePedidoInput!): Pedido! @visible(to: PORTAL)
            deletePedido(id: ID!): Pedido! @visible(to: PORTAL)
        }
    `
]

const transformInput = variablesTransformation([
    transformVariable("input.dealerId", "input.dealer", fetchItemTransform(User))
  ])
  
const createActionPedido = transformInput(createAction(Pedido))



const createPedido = async (_, { input }, { user }) => {
    const Pedido = await createActionPedido(_,{input},{user})
    return Pedido
}

const updatePedido = updateAction(Pedido)
// const createPedido = createAction(Pedido)

const findPedidoByUser = async (_, args) => {
  const res = await Pedido.find({where:{dealer :args.userId, deletedAt:null}})
  
  return paginatedResult(res.length,args.page,args.pageSize,res)
}

export const resolvers = {
  Query: {
    pedido: findAction(Pedido),
    findPedidoByUser,
    pedidos: indexAction(Pedido, { id: "ASC" })
  },
  Mutation: {
    createPedido,
    updatePedido,
    deletePedido: deleteAction(Pedido),
   },
  Pedido: {
    dealer: manyToOneResolver(User, "dealerId")
  }
}