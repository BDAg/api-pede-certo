import { SchemaDirectiveVisitor } from "graphql-tools"
import { defaultFieldResolver } from "graphql"
import UnauthorizedError from "../../errors/UnauthorizedError"

const AUDIENCE = {
  ADMIN: "ADMIN",
  PORTAL: "PORTAL",
  PUBLIC: "PUBLIC"
}

export default class VisibleDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { to } = this.args
    const { resolve = defaultFieldResolver } = field

    field.resolve = async function(...args) {
      const context = args[2]

      if (context.user && context.user.isBlocked) {
        throw new UnauthorizedError("Blocked User")
      }

      switch (to) {
        case AUDIENCE.ADMIN:
          if (!context.user || !context.user.admin) {
            throw new UnauthorizedError()
          }
          break
        case AUDIENCE.PORTAL:
          if (!context.user) {
            throw new UnauthorizedError()
          }
      }

      const result = await resolve.apply(this, args)
      return result
    }
  }
}
