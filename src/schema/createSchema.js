import { makeExecutableSchema } from "graphql-tools"
import { flow, get, compact, flatten, map } from "lodash/fp"
import VisibleDirective from "./directives/VisibleDirective"

export default function createSchema(modules) {
  const types = flow(
    map(get("types")),
    compact,
    flatten
  )(modules)
  const resolvers = modules.map(m => m.resolvers).filter(Boolean)

  const schema = `
    schema {
      query: Query,
      mutation: Mutation
    }
    type Query {
      _empty: String
    }
    type Mutation {
      _empty: String
    }
  `

  return makeExecutableSchema({
    resolvers,
    typeDefs: [schema, ...types],
    schemaDirectives: {
      visible: VisibleDirective
    }
  })
}
