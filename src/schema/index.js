import { gql } from "apollo-server-express"
import { flow, get, compact, flatten, map } from "lodash/fp"
import VisibleDirective from "./directives/VisibleDirective"
import modules from "./modules"

const defaultTypes = gql`
  type Query
  type Mutation
`

const types = flow(
  map(get("types")),
  compact,
  flatten
)(modules)

export const typeDefs = [defaultTypes, ...types]

export const resolvers = modules.map(m => m.resolvers).filter(Boolean)

export const directives = {
  visible: VisibleDirective
}
