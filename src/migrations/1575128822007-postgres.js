export class postgres1575128822007 {

    async up(queryRunner){
        await queryRunner.query(`CREATE TABLE "produto" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP DEFAULT null, "nome" character varying NOT NULL, "descricao" character varying, "preco" numeric NOT NULL, "altura" numeric, "largura" numeric, "profundaide" numeric, "image_src" character varying, "categoriaId" integer, CONSTRAINT "PK_99c4351f9168c50c0736e6a66be" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "categoria" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP DEFAULT null, "nome" character varying NOT NULL, "descricao" character varying, CONSTRAINT "PK_f027836b77b84fb4c3a374dc70d" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TYPE "user_role_enum" AS ENUM('DEALER', 'ADMIN')`, undefined);
        await queryRunner.query(`CREATE TABLE "user" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP DEFAULT null, "email" character varying NOT NULL, "phone" character varying, "cnpj" character varying, "role" "user_role_enum" NOT NULL, "passwordHash" character varying, "sessionId" character varying, "name" character varying NOT NULL, CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE ("email"), CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE TABLE "pedido" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP DEFAULT null, "preco_total" numeric, "data_pedido" date, "dealerId" integer, CONSTRAINT "PK_af8d8b3d07fae559c37f56b3f43" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_DEALER_ORDER" ON "pedido" ("dealerId") `, undefined);
        await queryRunner.query(`CREATE TABLE "pedido__produto" ("id" SERIAL NOT NULL, "createdAt" TIMESTAMP NOT NULL DEFAULT now(), "updatedAt" TIMESTAMP NOT NULL DEFAULT now(), "deletedAt" TIMESTAMP DEFAULT null, "preco" numeric, "quantidade" integer, "pedidoId" integer, "produtoId" integer, CONSTRAINT "PK_fc908b67d5e9fefd99dbd0d4744" PRIMARY KEY ("id"))`, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_PEDIDO_ORDER" ON "pedido__produto" ("pedidoId") `, undefined);
        await queryRunner.query(`CREATE INDEX "IDX_PRODUTO_ORDER" ON "pedido__produto" ("produtoId") `, undefined);
        await queryRunner.query(`ALTER TABLE "produto" ADD CONSTRAINT "FK_8a1e81267ae184590ce1ee9a39b" FOREIGN KEY ("categoriaId") REFERENCES "categoria"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "pedido" ADD CONSTRAINT "FK_013cfb67abc60153824bf516a82" FOREIGN KEY ("dealerId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "pedido__produto" ADD CONSTRAINT "FK_bdf9b0f1368c9f8bf100f4524ad" FOREIGN KEY ("pedidoId") REFERENCES "pedido"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "pedido__produto" ADD CONSTRAINT "FK_940f2369154a418d90b7f0b430b" FOREIGN KEY ("produtoId") REFERENCES "produto"("id") ON DELETE CASCADE ON UPDATE NO ACTION`, undefined);
    }

    async down(queryRunner){
        await queryRunner.query(`ALTER TABLE "pedido__produto" DROP CONSTRAINT "FK_940f2369154a418d90b7f0b430b"`, undefined);
        await queryRunner.query(`ALTER TABLE "pedido__produto" DROP CONSTRAINT "FK_bdf9b0f1368c9f8bf100f4524ad"`, undefined);
        await queryRunner.query(`ALTER TABLE "pedido" DROP CONSTRAINT "FK_013cfb67abc60153824bf516a82"`, undefined);
        await queryRunner.query(`ALTER TABLE "produto" DROP CONSTRAINT "FK_8a1e81267ae184590ce1ee9a39b"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_PRODUTO_ORDER"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_PEDIDO_ORDER"`, undefined);
        await queryRunner.query(`DROP TABLE "pedido__produto"`, undefined);
        await queryRunner.query(`DROP INDEX "IDX_DEALER_ORDER"`, undefined);
        await queryRunner.query(`DROP TABLE "pedido"`, undefined);
        await queryRunner.query(`DROP TABLE "user"`, undefined);
        await queryRunner.query(`DROP TYPE "user_role_enum"`, undefined);
        await queryRunner.query(`DROP TABLE "categoria"`, undefined);
        await queryRunner.query(`DROP TABLE "produto"`, undefined);
    }

}
