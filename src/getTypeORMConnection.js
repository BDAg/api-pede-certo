import "reflect-metadata"
import "../bootstrap"
import { createConnection } from "typeorm"

import ormconfig from "../ormconfig"

let connection

export default async function getTypeORMConnection(options = {}) {
  if (!connection) {
    connection = await createConnection({ ...ormconfig, ...options })
  }

  return connection
}
