import { Entity, Column, Index, ManyToOne, RelationId, ManyToMany, JoinTable } from "typeorm";
import ApplicationEntity from "./ApplicationEntity";
import User from "./User";
import Produto from "./Produto";
import { get } from "lodash/fp";

@Entity()
export default class Pedido extends ApplicationEntity {

  @Index("IDX_DEALER_ORDER")
  @ManyToOne(_ => User, { onDelete: "CASCADE" })
  dealer = undefined

  @RelationId(get("dealer"))
  dealerId = undefined

  @Column("decimal", { nullable: true })
  preco_total = undefined

  @Column('date',{nullable: true })
  data_pedido = undefined

}
