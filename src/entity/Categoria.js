import { Entity, Column, OneToMany } from "typeorm";
import ApplicationEntity from "./ApplicationEntity";
import Produto from "./Produto";

@Entity()
export default class Categoria extends ApplicationEntity {
    @Column("varchar", { nullable: false })
    nome = undefined

    @Column("varchar", { nullable: true })
    descricao = undefined

  // @ManyToOne(_ => Categoria, { onDelete: "CASCADE" })
  // categoria = undefined
  //
  // @RelationId(get("categoria"))
  // categoriaId = undefined

}
