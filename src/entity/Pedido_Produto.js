import { Entity, Column, Index, ManyToOne, RelationId, ManyToMany, JoinTable } from "typeorm";
import ApplicationEntity from "./ApplicationEntity";
import Pedido from "./Pedido";
import Produto from "./Produto";
import { get } from "lodash/fp";

@Entity()
export default class Pedido_Produto extends ApplicationEntity {

  @Index("IDX_PEDIDO_ORDER")
  @ManyToOne(_ => Pedido, { onDelete: "CASCADE" })
  pedido = undefined

  @RelationId(get("pedido"))
  pedidoId = undefined

  @Column("decimal", { nullable: true })
  preco = undefined

  @Column("int", { nullable: true })
  quantidade = undefined

  @Index("IDX_PRODUTO_ORDER")
  @ManyToOne(_ => Produto, { onDelete: "CASCADE" })
  produto = undefined

  @RelationId(get("produto"))
  produtoId = undefined

}