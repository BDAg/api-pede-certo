import {Entity, Column, ManyToOne, RelationId} from "typeorm";
import ApplicationEntity from "./ApplicationEntity";
import Categoria from "./Categoria";
import { get } from "lodash/fp";

@Entity()
export default class Produto extends ApplicationEntity {
  @ManyToOne(_ => Categoria, { onDelete: "CASCADE" })
  categoria = Categoria

  @RelationId(get("categoria"))
  categoriaId = undefined

  @Column("varchar", { nullable: false })
  nome = undefined

  @Column("varchar", { nullable: true })
  descricao = undefined

  @Column("decimal", { nullable: false })
  preco = undefined

  @Column("decimal", { nullable: false })
  peso = undefined
  
  @Column("decimal", { nullable: true })
  altura = undefined

  @Column("decimal", { nullable: true })
  largura = undefined

  @Column("decimal", { nullable: true })
  profundidade = undefined

  @Column("varchar", { nullable: true })
  image_src = undefined

  
}
