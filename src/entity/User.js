import { get, flow, isEqual, negate } from "lodash/fp"
import { Entity, Column, BeforeInsert, BeforeUpdate } from "typeorm"
import * as bcrypt from "bcrypt"
import { IsEmail, MinLength, IsOptional, ValidateIf } from "class-validator"
import { isTest } from "../../environment"
import ApplicationEntity from "./ApplicationEntity"

const SALT_ROUNDS = isTest ? 1 : 10

@Entity()
export default class User extends ApplicationEntity {
  static findByEmailForAuthentication(email) {
    return User.findOne({ email: email.toLowerCase() })
  }

  @Column("varchar", { unique: true })
  @IsEmail()
  email = undefined

  @Column("varchar", { nullable: true })
  phone = undefined

  @Column("varchar", { nullable: true })
  cnpj = undefined

  @Column("enum", {
    enum: ["DEALER", "ADMIN"],
    nullable: false
  })
  role = undefined

  @Column("varchar", { nullable: true })
  passwordHash = undefined

  @Column("varchar", { nullable: true })
  sessionId = undefined

  @Column("varchar", { nullable: false })
  name = undefined

  @ValidateIf(
    flow(
      get("admin"),
      negate(isEqual(true))
    )
  )

  @BeforeInsert()
  @BeforeUpdate()
  async encryptPassword() {
    if (this.password) {
      this.passwordHash = await bcrypt.hash(this.password, SALT_ROUNDS)
    }
  }

  @BeforeInsert()
  @BeforeUpdate()
  convertEmailToLowerCase() {
    if (this.email) {
      this.email = this.email.toLowerCase()
    }
  }

  @IsOptional(get("id"))
  @MinLength(6)
  get password() {
    return this._password
  }

  set password(password) {
    this._password = password

    // We have to clear the password hash otherwise typeorm
    // won't recognize the model has changed and thus won't
    // trigger the @BeforeUpdate hook
    this.passwordHash = null
  }

  get admin() {
    return this.role === "ADMIN"
  }

  validatePassword(plainTextPassword) {
    return bcrypt.compare(plainTextPassword, this.passwordHash)
  }

  async generateSessionId() {
    this.sessionId = Math.round(
      new Date().valueOf() * Math.random() * 100000
    ).toString()
    await this.save()
  }

  async removeSessionId() {
    this.sessionId = null
    await this.save()
  }
}
