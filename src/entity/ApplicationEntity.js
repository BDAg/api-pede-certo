import {
    PrimaryGeneratedColumn,
    BaseEntity,
    UpdateDateColumn,
    CreateDateColumn,
    Column
  } from "typeorm"
  
  export default class ApplicationEntity extends BaseEntity {
    @PrimaryGeneratedColumn() id = undefined
  
    @CreateDateColumn() createdAt = undefined
  
    @UpdateDateColumn() updatedAt = undefined
  
    @Column("timestamp", { default: null })
    deletedAt = undefined
  }
  