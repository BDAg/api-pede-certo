import { values } from "lodash/fp"

export default class BadInputError extends Error {
  code = 400

  constructor(errors) {
    super("BadInput")
    this.extensions = {
      problems: errors.map(error => ({
        path: error.property,
        messages: values(error.constraints)
      }))
    }
  }
}
