export default class UnauthorizedError extends Error {
  code = 401

  constructor(errors) {
    if (errors) {
      super(`Unauthorized ${errors}`)
    } else {
      super("Unauthorized")
    }
  }
}
