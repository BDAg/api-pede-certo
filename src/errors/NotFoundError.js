export default class NotFoundError extends Error {
  code = 404

  constructor(Entity, id) {
    super(`NotFound - ${Entity.name}:${[].concat(id).join("|")}`)
  }
}
