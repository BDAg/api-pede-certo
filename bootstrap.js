const { isProduction } = require("./environment")

if (!isProduction) {
  require("dotenv").config()
}

if (process.env.BLOCKED_AT === "true") {
  require("blocked-at")(
    (time, stack) => {
      console.log(`Blocked for ${time}ms, operation started here:`, stack)
    },
    { threshold: 100, trimFalsePositives: false }
  )
}
