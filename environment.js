const environment = "development"
const isDevelopment = environment === "development"

module.exports = {
  environment,
  isDevelopment,
}
